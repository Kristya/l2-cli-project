import "./my-contact-card.component.css";
import { fromEvent } from "rxjs";

export class MyContactCard extends HTMLElement {
  /**
   * Lifecycle hook - компонент создан в DOM
   */
  connectedCallback() {
    this.innerHTML = `
  <div class="contact">
    <div>
      <div id="form" class="contact-form"></div>
      <button class="button">Сохранить и отправить</button>
    </div>
    <div id="card"></div>
  </div>`;

    // this.querySelector("button").addEventListener(
    //   "click",
    //   this.setLocalInfo.bind(this)
    // );
    const source = fromEvent(this.querySelector("button"), "click");
    source.subscribe(event => this.getLocaleInfo(event));

    this.render();
  }

  /**
   * Сохранение данных в локальном хранилище
   * @param {MouseEvent} event
   */
  async setLocalInfo(event) {
    const data = {};
    event.target.parentElement
      .querySelectorAll("input, textarea")
      .forEach(el => (data[el.name] = el.value));

    localStorage.setItem("my-contact-card", JSON.stringify(data));
    this.render();

    const response = await fetch("https://uxd.ispsystem.net/savecontactinfo", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });

    if (response.status !== 200) {
      console.warn(response);
    }
  }

  /**
   * Отрисовка изменяемых частей
   */
  render() {
    const contact = this.getLocalInfo();

    this.querySelector("#form").innerHTML = `
    <input type="text" name="name" placeholder="" required /> <label>Имя</label>
    <input type="text" name="nickname" placeholder="" required /> <label>Никнейм</label>
    <input type="text" name="email" placeholder="" required /> <label>Email</label>
    <input type="phone" name="phone" placeholder="" required /> <label>Телефон</label>
    <input type="text" name="direct" placeholder="" required /> <label>Направление</label>
    <textarea placeholder="" name="info" required></textarea>
    <label>Кратко о себе</label>
  `;
    this.querySelectorAll("input, textarea").forEach(
      el => (el.value = contact[el.name])
    );

    this.querySelector("#card").innerHTML = contact.isEmpty
      ? ""
      : `
      <div class="contact-card">
        <div class="contact-profile">
        ${contact.nickname.slice(0, 2).toUpperCase()}
        </div>
        <div class="contact-info-container">
          <div class="contact-info">
            <div class="material-icons">person</div>
            <div class="contact-data">${contact.name}</div>
          </div>
          <div class="contact-info">
            <div class="material-icons">sentiment_satisfied</div>
            <div class="contact-data">${contact.nickname}</div>
          </div>
          <div class="contact-info">
            <div class="material-icons">email</div>
            <div class="contact-data">${contact.email}</div>
          </div>
          <div class="contact-info">
            <div class="material-icons">phone</div>
            <div class="contact-data">${contact.phone}</div>
          </div>
          <div class="contact-info">
            <div class="material-icons">search</div>
            <div class="contact-data">${contact.direct}</div>
          </div>
          <div class="contact-data-container">
            <div class="material-icons">notes</div>
            <div class="contact-data">
            ${contact.info}
            </div>
          </div>
        </div>
      </div>
    `;
  }

  /**
   * Получение данных из локального хранилища
   */
  getLocalInfo() {
    const myContactCardInfo = localStorage.getItem("my-contact-card");
    const contact = myContactCardInfo
      ? JSON.parse(myContactCardInfo)
      : {
          name: "",
          nickname: "",
          email: "",
          phone: "",
          direct: "",
          info: ""
        };

    if (Object.keys(contact).every(k => contact[k] === "")) {
      contact.isEmpty = true;
    }
    return contact;
  }
}

// определение custom element
let myContactCardTag = "my-contact-card";
try {
  customElements.define(myContactCardTag, MyContactCard);
} catch (error) {
  /////////////////////////////////////////////////////////////////////
  // нужно только для режима разработки с HMR
  if (process.env.NODE_ENV === "development") {
    document.body.removeChild(document.querySelector(myContactCardTag));
    myContactCardTag = `my-contact-card__${new Date().getTime()}`;
    customElements.define(myContactCardTag, MyContactCard);
    let el = new MyContactCard();
    document.body.appendChild(el);
  }
  /////////////////////////////////////////////////////////////////////
}
